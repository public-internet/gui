// +build linux freebsd netbsd openbsd dragonfly

package setproxy
import "bytes"
import "errors"
import "net/url"
import "os/exec"

// Sets proxy for GNOME
// FIXME: only works for GNOME, not KDE, Xfce, etc.
// TODO: use C bindings

func ( self * State )Proxy( proxy url.URL )error {
	var err error
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	var write func( struct{
		mode string
		http struct{
			host string
			port string
		}
		https struct{
			host string
			port string
		}
		ftp struct{
			host string
			port string
		}
		socks struct{
			host string
			port string
		}
	} )error
	_ , err = exec.LookPath( "gsettings" )
	if err != nil {
		return wrongSystem }
	if self.restore != nil {
		err = self.Restore( )
		if err != nil {
			return err }
	}

	// Get original settings
	var original struct{
		mode string
		http struct{
			host string
			port string
		}
		https struct{
			host string
			port string
		}
		ftp struct{
			host string
			port string
		}
		socks struct{
			host string
			port string
		}
	}

	err = ( & exec.Cmd{
		Path : mu( exec.LookPath( "gsettings" ) )[ 0 ].( string ) ,
		Args : [ ]string{
			"gsettings" ,
			"get" ,
			"org.gnome.system.proxy" ,
			"mode" ,
		} ,
		Stdout : & stdout ,
		Stderr : & stderr ,
	} ).Run( )
	if err != nil {
		return errors.New( err.Error( ) + ": " + stderr.String( ) ) }
	original.mode = stdout.String( )[ 1 : len( stdout.String( ) ) - 2 ]
	stdout.Reset( )

	for title , place := range map[ string ]*struct{
			host string
			port string
	}{
		"http" : & original.http ,
		"https" : & original.https ,
		"ftp" : & original.ftp ,
		"socks" : & original.socks ,
	} {

		err = ( & exec.Cmd{
			Path : mu( exec.LookPath( "gsettings" ) )[ 0 ].( string ) ,
			Args : [ ]string{
				"gsettings" ,
				"get" ,
				"org.gnome.system.proxy." + title ,
				"host" ,
			} ,
			Stdout : & stdout ,
			Stderr : & stderr ,
		} ).Run( )
		if err != nil {
			return errors.New( err.Error( ) + ": " + stderr.String( ) ) }
		place.host = stdout.String( )[ 1 : len( stdout.String( ) ) - 2 ]
		stdout.Reset( )

		err = ( & exec.Cmd{
			Path : mu( exec.LookPath( "gsettings" ) )[ 0 ].( string ) ,
			Args : [ ]string{
				"gsettings" ,
				"get" ,
				"org.gnome.system.proxy." + title ,
				"port" ,
			} ,
			Stdout : & stdout ,
			Stderr : & stderr ,
		} ).Run( )
		if err != nil {
			return errors.New( err.Error( ) + ": " + stderr.String( ) ) }
		place.port = stdout.String( )
		stdout.Reset( )

	}

	self.restore = func( )error{
		return write( original )
	}

	// Writing
	write = func( config struct{
		mode string
		http struct{
			host string
			port string
		}
		https struct{
			host string
			port string
		}
		ftp struct{
			host string
			port string
		}
		socks struct{
			host string
			port string
		}
	} )error{

		err = ( & exec.Cmd{
			Path : mu( exec.LookPath( "gsettings" ) )[ 0 ].( string ) ,
			Args : [ ]string{
				"gsettings" ,
				"set" ,
				"org.gnome.system.proxy" ,
				"mode" ,
				config.mode ,
			} ,
			Stderr : & stderr ,
		} ).Run( )
		if err != nil {
			return errors.New( err.Error( ) + ": " + stderr.String( ) ) }

		for title , place := range map[ string ]struct{
			host string
			port string
		}{
			"http" : config.http ,
			"https" : config.https ,
			"ftp" : config.ftp ,
			"socks" : config.socks ,
		} {

			err = ( & exec.Cmd{
				Path : mu( exec.LookPath( "gsettings" ) )[ 0 ].( string ) ,
				Args : [ ]string{
					"gsettings" ,
					"set" ,
					"org.gnome.system.proxy." + title ,
					"host" ,
					place.host ,
				} ,
				Stderr : & stderr ,
			} ).Run( )
			if err != nil {
				return errors.New( err.Error( ) + ": " + stderr.String( ) ) }

			err = ( & exec.Cmd{
				Path : mu( exec.LookPath( "gsettings" ) )[ 0 ].( string ) ,
				Args : [ ]string{
					"gsettings" ,
					"set" ,
					"org.gnome.system.proxy." + title ,
					"port" ,
					place.port ,
				} ,
				Stderr : & stderr ,
			} ).Run( )
			if err != nil {
				return errors.New( err.Error( ) + ": " + stderr.String( ) ) }

		}

		return nil
	}

	if proxy.Scheme == "http" {
		err = write( struct{
			mode string
			http struct{
				host string
				port string
			}
			https struct{
				host string
				port string
			}
			ftp struct{
				host string
				port string
			}
			socks struct{
				host string
				port string
			}
		}{
			mode : "manual" ,
			http : struct{
				host string
				port string
			}{
				host : proxy.Hostname( ) ,
				port : proxy.Port( ) ,
			} ,
			https : struct{
				host string
				port string
			}{
				host : proxy.Hostname( ) ,
				port : proxy.Port( ) ,
			} ,
			ftp : struct{
				host string
				port string
			}{
				host : proxy.Hostname( ) ,
				port : proxy.Port( ) ,
			} ,
			socks : struct{
				host string
				port string
			}{
				host : "" ,
				port : "0" ,
			} ,
		} )

	} else if proxy.Scheme == "socks" ||
	    proxy.Scheme == "socks5" {
		err = write( struct{
			mode string
			http struct{
				host string
				port string
			}
			https struct{
				host string
				port string
			}
			ftp struct{
				host string
				port string
			}
			socks struct{
				host string
				port string
			}
		}{
			mode : "manual" ,
			http : struct{
				host string
				port string
			}{
				host : "" ,
				port : "0" ,
			} ,
			https : struct{
				host string
				port string
			}{
				host : "" ,
				port : "0" ,
			} ,
			ftp : struct{
				host string
				port string
			}{
				host : "" ,
				port : "0" ,
			} ,
			socks : struct{
				host string
				port string
			}{
				host : proxy.Hostname( ) ,
				port : proxy.Port( ) ,
			} ,
		} )	
	
	} else {
		return errors.New( "I dont know proxy type " + proxy.Scheme )

	}
	if err != nil {
		return err }

	return nil
}


