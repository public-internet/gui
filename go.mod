module gitlab.com/public-internet/gui

go 1.17

require (
	github.com/KonaArctic/Token v0.0.0-20211221234727-ba4a8769400a // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/ipfs/go-log v1.0.4 // indirect
	github.com/ipfs/go-log/v2 v2.1.1 // indirect
	github.com/libp2p/go-buffer-pool v0.0.2 // indirect
	github.com/libp2p/go-mplex v0.3.0 // indirect
	github.com/multiformats/go-varint v0.0.6 // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/refraction-networking/utls v1.0.0 // indirect
	github.com/webview/webview v0.0.0-20210330151455-f540d88dde4e // indirect
	gitlab.com/public-internet/client v0.0.0-20211229075306-6e1426bae9a2 // indirect
	gitlab.com/public-internet/http-base v0.0.0-20211225040118-647abcd2fb6a // indirect
	gitlab.com/public-internet/tcpomux v0.0.0-20211229061435-6b413fc6eb5a // indirect
	go.uber.org/atomic v1.6.0 // indirect
	go.uber.org/multierr v1.5.0 // indirect
	go.uber.org/zap v1.15.0 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
