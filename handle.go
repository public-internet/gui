package main
import "embed"
import "io"
import paclient "gitlab.com/public-internet/client"
import "net/http"

//go:embed *.htm *.css assets
var webapp embed.FS

type handle struct{
	webapp embed.FS
	bypass * paclient.Bypass
}

func ( self handle )ServeHTTP( response http.ResponseWriter, request * http.Request  ) {

	// Windows does not accept connections to localhost https://github.com/webview/webview/issues/599
	// MitM proxy traffic instead
	if request.URL.Host == "gui.public.net" {
		content , err := webapp.ReadFile( request.URL.Path[ 1 : ] )
		if err != nil {
			response.WriteHeader( http.StatusNotFound )
			_ , err = response.Write( [ ]byte( err.Error( ) + "\r\n" ) )
			return }
		if request.URL.Path[ len( request.URL.Path ) - 4 : ] == ".svg" {
			response.Header( )[ "Content-Type" ] = [ ]string{ "image/svg+xml" , }	// Go cant guess correctly
		}
		response.WriteHeader( http.StatusOK )
		_ , err = response.Write( content )
		return

	// Some assets are massive and not strictly required. Load them on-the-fly.
	} else if request.URL.Host == "asset.public.net" {
		upstream , err := ( & http.Client{ } ).Get( "https://061112.netlify.app/" + request.URL.Path )
		if err != nil {
			response.WriteHeader( http.StatusBadGateway )
			_ , err = response.Write( [ ]byte( err.Error( ) ) ) }
		response.Header( )[ "Access-Control-Allow-Origin" ] = [ ]string{ "*" }
		response.WriteHeader( http.StatusOK )
		_ , err = io.Copy( response , upstream.Body )
		return

	} else {
		self.bypass.ServeHTTP( response , request )
		return

	}
}

