package setproxy
import "errors"

var wrongSystem error = errors.New( "wrong system" )

type State struct{
	restore func( )error
}

func ( self * State )Restore( )error {
	var err error
	if self.restore == nil {
		return errors.New( "Did you call Proxy( ) yet?" ) }
	err = self.restore( )
	if err == nil {
		self.restore = nil
	}
	return err
}

