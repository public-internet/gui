package main
import "github.com/webview/webview"
import paclient "gitlab.com/public-internet/client"
import "gitlab.com/public-internet/gui/setproxy"
import "net/http"
import "net/url"
import "os"

func main( ) {
	var err error
	var handle handle
	var bypass paclient.Bypass
	var window webview.WebView = webview.New( true )
	_ , err = os.Stderr.Write( [ ]byte( "L Copyright (c) 2021 Kona Arctic. All rights reserved. ABSOLUTELY NO WARRANTY! https://akona.me mailto:arcticjieer@gmail.com\r\n" ) )
	if err != nil {
		os.Exit( 10 ) }

	// TODO Dont hard-code
	handle.bypass = & bypass
	bypass.Edges = [ ]paclient.Edge{
		paclient.Edge{
			Country : "United States" ,
			Method : "direct" ,
			Cookie : "public-internet-america.herokuapp.com" ,
		} ,
		paclient.Edge{
			Country : "European Union" ,
			Method : "direct" ,
			Cookie : "public-internet-europe.herokuapp.com" ,
		} ,
	}

	//
	var proxstate setproxy.State
	err = proxstate.Proxy( url.URL{
		Scheme : "http" ,
		Host : "localhost:24447" ,
	} )
	if err != nil {
		_ , _ = os.Stderr.Write( [ ]byte( "F " + err.Error( ) ) )
		os.Exit( 78 ) }
	defer func( ){
		err = proxstate.Restore( )
		if err != nil {
			_ , _ = os.Stderr.Write( [ ]byte( "F " + err.Error( ) ) )
			os.Exit( 75 ) }
	}( )

	//
	bindadd( window , & bypass )

	// Start server
	var server http.Server
	server.Handler = handle
	server.Addr = "[::]:24447"
	go func( ){
		err = server.ListenAndServe( )
		if err != nil {
			_ , _ = os.Stderr.Write( [ ]byte( "F " + err.Error( ) ) )
			os.Exit( 11 ) }
	}( )

	// Open window
	window.SetTitle( "Public Internet" )
	window.SetSize( int( sizing.width ) , int( sizing.height ) , webview.HintFixed )
	window.Navigate( "http://gui.public.net/index.htm#main" )
	window.Run( )

}

