package setproxy
import "net/url"
import "testing"

// Only works on GNOME
func TestGnome( test * testing.T ) {
	var err error
	var state State

	err = state.Proxy( url.URL{
		Scheme : "http" ,
		Host : "localhost:8080" ,
	} )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }

	err = state.Proxy( url.URL{
		Scheme : "http" ,
		Host : "example.com:80" ,
	} )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }

	err = state.Restore( )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }

	return
}

