package main
import "bytes"
import "github.com/webview/webview"
import paclient "gitlab.com/public-internet/client"
import "os"

func bindadd( window webview.WebView , bypass * paclient.Bypass ) {
	var err error

	window.Bind( "connect" , func( )error {
		_ , err = bypass.Connect( )
//		if err != nil {
//			_ , err = os.Stderr.Write( [ ]byte( err.Error( ) + "\r\n" ) )
//			os.Exit( 44 ) }
		return err
	} )

	window.Bind( "country" , func( country string )error {
		bypass.Country = country
		return err
	} )
	
	window.Bind( "terminate" , func( )error {
		err = bypass.Terminate( )
//		if err != nil {
//			_ , err = os.Stderr.Write( [ ]byte( err.Error( ) + "\r\n" ) )
//			os.Exit( 43 ) }
		return err
	} )

	// Stderr logs
	var stderr os.File = * os.Stderr
	reader , writer , err := os.Pipe( )
	if err != nil {
		_ , _ = stderr.Write( [ ]byte( "F " + err.Error( ) + "\r\n" ) )
		os.Exit( 21 ) }
	os.Stderr = writer

	// window.Eval faults on Windows and IDK why
	// use polling instead
	var buffer bytes.Buffer
	go func( ){
		for {
			var tempbuf [ ]byte = [ ]byte{ 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 }
			length , err := reader.Read( tempbuf )
			if err != nil {
				_ , err = stderr.Write( [ ]byte( "F " + err.Error( ) + "\r\n" ) )
				if err != nil {
					os.Exit( 53 ) }
				os.Exit( 54 ) }
			tempbuf = tempbuf[ 0 : length ]
			_ , err = stderr.Write( tempbuf )
			if err != nil {
				os.Exit( 55 ) }
			_ , err = buffer.Write( tempbuf )
			if err != nil {
				_ , err = stderr.Write( [ ]byte( "F " + err.Error( ) + "\r\n" ) )
				if err != nil {
					os.Exit( 56 ) }
				os.Exit( 58 ) }
		}
	}( )
	window.Bind( "stderr_poll" , func( )string {
		bufstr := buffer.String( )
		buffer.Reset( )
		return bufstr
	} )

}

