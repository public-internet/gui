// +build windows

package setproxy
import "golang.org/x/sys/windows/registry"
import "net/url"

func ( self * State )Proxy( proxy url.URL )error {
	var err error
	var regkey registry.Key
	var write func( struct{
		enable uint64
		server string
	} )error

	// Get originals
	var original struct{
		enable uint64
		server string
	}
	regkey , err = registry.OpenKey( registry.CURRENT_USER , `SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings` , registry.QUERY_VALUE )
	if err != nil {
		return err }
	defer regkey.Close( )
	original.enable , _ , err = regkey.GetIntegerValue( "ProxyEnable" )
	if err != nil {
		return err }
	original.server , _ , err = regkey.GetStringValue( "ProxyServer" )
	if err != nil {
		return err }
	self.restore = func( )error{
		return write( original )
	}

	// Write config
	write = func( config struct{
		enable uint64
		server string
	} )error{
		regkey , err = registry.OpenKey( registry.CURRENT_USER , `SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings` , registry.SET_VALUE )
		if err != nil {
			return err }
		defer regkey.Close( )
		err = regkey.SetDWordValue( "ProxyEnable" , uint32( config.enable ) )
		if err != nil {
			return err }
		err = regkey.SetStringValue( "ProxyServer" , config.server )
		if err != nil {
			return err }
		return nil
	}
	write( struct{
		enable uint64
		server string
	}{
		enable : 1 ,
		server : proxy.String( ) ,
	} )

	return nil
}
